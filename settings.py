valid_chars = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8, 'i': 9, 'j': 10, 'k': 11,
               'l': 12, 'm': 13, 'n': 14, 'o': 15, 'p': 16, 'q': 17, 'r': 18, 's': 19, 't': 20, 'u': 21,
               'v': 22, 'w': 23, 'x': 24, 'y': 25, 'z': 26, '0': 27, '1': 28, '2': 29, '3': 30, '4': 31,
               '5': 32, '6': 33, '7': 34, '8': 35, '9': 36, '-': 37, '_': 38, '.': 39}

# Define your dga group map here, e.g.:
group_map = {0: 'benign', 1: 'bamital', 2: 'banjori', 3: 'bazarloader', 4: 'bedep', 5: 'beebone', 6: 'blackhole',
             7: 'bobax', 8: 'ccleaner', 9: 'chaes', 10: 'chinad', 11: 'chir', 12: 'conficker', 13: 'corebot',
             14: 'cryptolocker', 15: 'darkshell', 16: 'darkwatchman', 17: 'diamondfox', 18: 'dircrypt', 19: 'dmsniff',
             20: 'dnsbenchmark', 21: 'dnschanger', 22: 'downloader', 23: 'dyre', 24: 'ebury', 25: 'ekforward',
             26: 'emotet', 27: 'enviserv', 28: 'feodo', 29: 'flubot', 30: 'fobber', 31: 'g01', 32: 'gameover',
             33: 'gameover_p2p', 34: 'gozi', 35: 'goznym', 36: 'gspy', 37: 'hesperbot', 38: 'infy', 39: 'kingminer',
             40: 'locky', 41: 'm0yv', 42: 'm0yvtdd', 43: 'madmax', 44: 'makloader', 45: 'matsnu', 46: 'mirai',
             47: 'modpack', 48: 'monerominer', 49: 'murofet', 50: 'murofetweekly', 51: 'mydoom', 52: 'necro',
             53: 'necurs', 54: 'nymaim', 55: 'nymaim2', 56: 'oderoor', 57: 'omexo', 58: 'padcrypt', 59: 'pandabanker',
             60: 'phorpiex', 61: 'pitou', 62: 'proslikefan', 63: 'pushdo', 64: 'pushdotid', 65: 'pykspa', 66: 'pykspa2',
             67: 'pykspa2s', 68: 'qadars', 69: 'qakbot', 70: 'qhost', 71: 'qsnatch', 72: 'ramdo', 73: 'ramnit',
             74: 'ranbyus', 75: 'randomloader', 76: 'redyms', 77: 'rovnix', 78: 'sharkbot', 79: 'shifu', 80: 'simda',
             81: 'sisron', 82: 'sphinx', 83: 'suppobox', 84: 'sutra', 85: 'symmi', 86: 'szribi', 87: 'tempedreve',
             88: 'tempedrevetdd', 89: 'tinba', 90: 'tinynuke', 91: 'tofsee', 92: 'torpig', 93: 'tsifiri', 94: 'ud2',
             95: 'ud3', 96: 'ud4', 97: 'urlzone', 98: 'vawtrak', 99: 'vidro', 100: 'vidrotid', 101: 'virut',
             102: 'volatilecedar', 103: 'wd', 104: 'xshellghost', 105: 'xxhex', 106: 'zloader'}

# Define your tld dict here, e.g.:
tlds = {'': 0, 'com': 1, "de": 2}

# Define your dataset paths here, e.g.:
DS_MODELS_PATH = "./datasets/mod.pkl"
DS_EXPLAINABILITY_PATH = "./datasets/ex.pkl"

maxlen = 253
max_features = len(valid_chars) + 1
nb_classes = len(group_map)
class_weighting_power = 0.2
